//
//  VCRegistro.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class VCLogin: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnConectar: UIButton!
    @IBOutlet weak var btnRegistro: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //SETEAR ESTILOS
        setStylesUI()
        
        //comprobar si ya estaba logeado
        comprobarSiLoged()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func comprobarSiLoged(){
        Auth.auth().addStateDidChangeListener { (auth, user) in
            
            if(user != nil){
                self.performSegue(withIdentifier: "segueLoginInitialChooser", sender: self)
            }
        }
    }
    
    
    @IBAction func loguearse(){
        do {
            try Auth.auth().signOut()
            print("elefanteverde")
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        Auth.auth().signIn(withEmail: (txtEmail.text)!, password: (txtPassword.text)!) {(user,error) in
            
            if(error==nil){
                
                // delay para que se muestre por pantalla que se ha registrado correctamente antes de redirigirse
                // a la pantalla de "inicio de sesión" de nuevo.
                /*
                 if (Auth.auth().currentUser?.isEmailVerified == true){
                 self.performSegue(withIdentifier: "segueLoginMain", sender: self)
                 }else{
                 */
                self.performSegue(withIdentifier: "segueLoginInitialChooser", sender: self)
         
            }else{
                print("Error en logueo", error!)
                //TODO ADVERTENCIA
            }
        }
    }
    
    @objc func esconderTeclado(){
        txtEmail?.endEditing(true)
        txtPassword?.endEditing(true)
    }
    
    func setStylesUI(){
        
        txtEmail.layer.borderWidth = 0
        
        
        //PARA PODER ESCONDER TECLADO
        let tap = UITapGestureRecognizer(target: self, action: #selector(esconderTeclado))
        self.view.addGestureRecognizer(tap)
    }
}
