//
//  VCGrupos.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import FirebaseAuth

class VCGrupos: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tablaGrupos: UITableView!
    
    var grupos = [Grupo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tablaGrupos.delegate = self
        tablaGrupos.dataSource = self
        
        cargarArrayGruposUser()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func cargarArrayGruposUser(){
        var email: String = (Auth.auth().currentUser?.email)!
        
        email = email.replacingOccurrences(of: ".", with: ";")
        
        DataHolder.sharedInstance.firDataBaseRef.child("usuarios").child(email).child("grupos").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            if value != nil {
                
                DataHolder.sharedInstance.gruposUser = value as? NSMutableDictionary
                
                for (key, _) in value! {
                    self.cargarGrupos(key: key as! String)
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func cargarGrupos(key:String) {
        DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            //print("VALUE: ",value)
            
            let grupo:Grupo = Grupo(dic: value!, name: key)

            //print("Grupo: ",grupo)
            
            self.grupos.append(grupo)
            
            print("CONT ANTES DE RECARGA: ",self.grupos.count)
            
            DataHolder.sharedInstance.grupo = self.grupos[0]
            self.performSegue(withIdentifier: "segueGruposToSelectedGrupo", sender: self)
            
            self.tablaGrupos.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
            
        }
        
        
    }
    
    //INICIO FUNCIONES TABLA
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grupos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = UITableViewCell()
        
        cell.textLabel?.text = grupos[indexPath.row].nombre
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataHolder.sharedInstance.grupo = grupos[indexPath.row]
        performSegue(withIdentifier: "segueGruposToSelectedGrupo", sender: self)
    }
    
    //FIN FUNCIONES TABLA

}
