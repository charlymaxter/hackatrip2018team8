//
//  VCPlanGanador.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import FirebaseAuth

class VCPlanGanador: UIViewController {
    
    var grupo:Grupo!
    @IBOutlet weak var lblActividad: UILabel!
    @IBOutlet weak var lblCiudad: UILabel!
    @IBOutlet weak var btnReservar: UIButton!
    @IBOutlet weak var isReservado: UILabel!
    
    var email: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        grupo = DataHolder.sharedInstance.grupo

        email = getEmail()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func getEmail() -> String{
        
        var temp = Auth.auth().currentUser?.email
        temp = temp?.replacingOccurrences(of: ".", with: ";")
        
        return temp!
    }
    
    func isMaster() -> Bool{
        if grupo.master == email {
            return true
        } else {
            return false
        }
    }
    
    func setStylesUI() {
        
        if isMaster() {
            
        } else {
            btnReservar.isEnabled = false
            btnReservar.isHidden = true
        }
        
        if grupo.reservado {
            isReservado.text = "¡Ya esta reservado!"
        }
    }
    
    @IBAction func reservar() {
        isReservado.text = "¡Ya esta reservado!"
        btnReservar.isEnabled = false
        btnReservar.isHidden = true
        DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(grupo.nombre).child("reservado").setValue(true)
    }
    
}
