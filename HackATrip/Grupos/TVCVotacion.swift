//
//  TVCVotacion.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit

class TVCVotacion: UITableViewCell {
    
    @IBOutlet weak var lblActividad: UILabel!
    @IBOutlet weak var lblCiudad: UILabel!
    @IBOutlet weak var lblFechaInicio: UILabel!
    @IBOutlet weak var lblFechaFin: UILabel!
    @IBOutlet weak var lblprecio: UILabel!
    @IBOutlet weak var lblhotel: UILabel!
    @IBOutlet weak var lblVotacion: UILabel!
    @IBOutlet weak var imgActividad: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
