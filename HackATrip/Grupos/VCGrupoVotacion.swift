//
//  VCGrupoVotacion.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import FirebaseAuth

class VCGrupoVotacion: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var pulsacion:Bool = false
    
    var grupo:Grupo?
    @IBOutlet weak var tablaPlan:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        grupo = DataHolder.sharedInstance.grupo
        //TABLA SELFS
        tablaPlan.delegate = self
        tablaPlan.dataSource = self
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        finalComprobation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    //INICIO FUNCIONES TABLA
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if grupo != nil {
            return (grupo?.planes.count)!
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TVCVotacion = tableView.dequeueReusableCell(withIdentifier: "TVCVotacion", for: indexPath) as! TVCVotacion
        
        let plan:Plan = (grupo?.planes[indexPath.row])!
        
        cell.lblActividad.text = plan.actividad
        cell.lblhotel.text = plan.hotel
        cell.lblCiudad.text = plan.ciudad
        cell.lblVotacion.text = plan.votos.description
        cell.lblprecio.text = plan.precio
        
        
        
        //FECHAS
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        cell.lblFechaFin.text = formatter.string(from: plan.fechaFin!)
        cell.lblFechaInicio.text = formatter.string(from: plan.fechaInicio!)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //FIN FUNCIONES TABLA
    
    
    @IBAction func votar(){
        if !pulsacion {
            let index = tablaPlan.indexPathForSelectedRow
            if index?.row != nil {
                if !(DataHolder.sharedInstance.gruposUser![grupo?.nombre] as! Bool) {
                    pulsacion = true
                    let plan: Plan = (grupo?.planes[(index?.row)!])!
                    let votos = plan.votos + 1
                    DataHolder.sharedInstance.gruposUser![grupo?.nombre] = true
                    DataHolder.sharedInstance.firDataBaseRef.child("grupos").child((grupo?.nombre)!).child("planes").child((index?.row.description)!).child("votos").setValue(votos)
                    
                    var email: String = (Auth.auth().currentUser?.email)!
                    email = email.replacingOccurrences(of: ".", with: ";")
                    DataHolder.sharedInstance.firDataBaseRef.child("usuarios").child(email).child("grupos").child((grupo?.nombre)!).setValue(true)
                    
                    (grupo?.planes[(index?.row)!] as! Plan).votos = votos
                    self.tablaPlan.reloadData()
                    
                    finalComprobation()
                }
            }
            
        } else {
            print("Ya voto")
        }
        
    }
    func comprobacionGanador() -> Bool {
        
        let planes = grupo?.planes
        
        var count = 0
        for plan in planes! {
            count = count + plan.votos
        }
        
        print("COUNT: ",count)
        print("Participantes: ",grupo?.participantes?.count)
        
        if count >= (grupo?.participantes?.count)! {
            return true
        } else {
            return false
        }
        
    }
    
    func avanzarGanador() {
        print("AVANZA")
        
        performSegue(withIdentifier: "segueVotacionReserva", sender: self)
    }
    
    func finalComprobation(){
        if comprobacionGanador() {
            //AVANZAR
            avanzarGanador()
        }
    }
}






