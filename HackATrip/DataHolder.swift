//
//  DataHolder.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import Firebase

class DataHolder: NSObject {
    
    static let sharedInstance: DataHolder = DataHolder()
    
    var firDataBaseRef: DatabaseReference!

    var grupo: Grupo?
    
    var nombreGrupo: String?
    
    var gruposUser: NSMutableDictionary?
    
    func initFirebase() {
        FirebaseApp.configure()
        firDataBaseRef=Database.database().reference()
        
        
    }
    
    
}
