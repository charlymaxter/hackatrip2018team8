//
//  VCRandom.swift
//  HackATrip
//
//  Created by Charly Maxter on 21/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import FirebaseAuth

class VCRandom: UIViewController {
    
    //VIEW DETALLES VIAJE
    @IBOutlet weak var viewDetalles: UIView!
    @IBOutlet weak var txtDiaInicio: UITextField!
    @IBOutlet weak var txtDiaFin: UITextField!
    @IBOutlet weak var txtMesInicio: UITextField!
    @IBOutlet weak var txtMesFin: UITextField!
    @IBOutlet weak var txtAnioInicio: UITextField!
    @IBOutlet weak var txtAnioFin: UITextField!
    @IBOutlet weak var txtnumPersonas: UITextField!
    @IBOutlet weak var txtMinMoney: UITextField!
    @IBOutlet weak var txtMaxMoney: UITextField!
    @IBOutlet weak var txtNombreGrupo: UITextField!
    
    //VIEW Random
    @IBOutlet weak var imgPlan: UIImageView!
    @IBOutlet weak var lblPrecio: UILabel!
    @IBOutlet weak var lblCiudad: UILabel!
    @IBOutlet weak var lblActividad: UILabel!
    @IBOutlet weak var lblHotel: UILabel!
    
    
    //OBJETOS
    var total:Int = 0
    var planes = [Plan]()
    var plan: Plan!
    var dictionary:NSMutableDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStylesUI()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func aceptarCampos() {
        cargarInfo()
        viewDetalles.isHidden = true
    }
    
    @IBAction func modificarCampos() {
        viewDetalles.isHidden = false
    }
    
    func cargarInfo() {
        let fechaInicio = crearStringFromFecha(tipo: "inicio")
        let fechaFin = crearStringFromFecha(tipo: "otro")
        ApiRest.sharedInstance.allRandom(Int(txtnumPersonas.text!)!, min_dinero: Int(txtMinMoney.text!)!, max_dinero: Int(txtMaxMoney.text!)!, fecha_inicio: fechaInicio, fecha_fin: fechaFin){
            (json: JSON) in
            if(json != nil){
                //print("json: ",json)
                self.processInfo(json: json)
            }else{
                
            }
        }
    }
    
    /*
     actividad = dic["actividad"] as! String
     ciudad = dic["ciudad"] as! String
     pais = dic["pais"] as! String
     precio = dic["precio"] as! String
     urlImagen = dic["urlImagenPlan"] as! String
     hotel = dic["hotel"] as! String
     votos = (dic["votos"] as! Int)
     */
    
    func processInfo(json:JSON){
        //CREAMOS EL PLAN
        let temp:NSArray = json["Plan"].arrayObject! as NSArray
        
        let temp2:NSDictionary = temp[0] as! NSDictionary
        if temp2["name_poi"] == nil || temp2["name_poi"] as! String == ""{
            //cargarInfo()
        } else {
            
            let dicRes:NSMutableDictionary = NSMutableDictionary()
            dicRes["pais"] = "España"
            dicRes["hotel"] = temp2["name_hotel"]
            dicRes["actividad"] = temp2["name_poi"]
            dicRes["ciudad"] = temp2["name_City"]
            dicRes["urlImagenPlan"] = temp2["picture_poi"]
            dicRes["precio"] = "120,47"
            dicRes["votos"] = 0
            dicRes["fechaInicio"] = crearStringFromFechaDMY(tipo: "inicio")
            dicRes["fechaFin"] = crearStringFromFechaDMY(tipo: "otro")
            plan = Plan(dic: dicRes)
            //print("CIUDAD: ",temp2["name_City"])
            
            
            
            //Cargamos la info en los labels
            DispatchQueue.main.async {
                self.lblPrecio.text = self.plan.precio
                self.lblHotel.text = self.plan.hotel
                self.lblCiudad.text = self.plan.ciudad
                self.lblActividad.text = self.plan.actividad
            }
        }
    }
    
    @IBAction func noGusta(){
        //Descartamos lo que nos dio y cargamos otros nuevos
        cargarInfo()
        
    }
    
    @IBAction func siGusta(){
        print(plan)
        planes.append(plan)
        total = total + 1
        if total >= 5 {
            print("+ 1 ", total)
            let grupo:Grupo = Grupo()
            let dic = crearDiccionario()
            grupo.master = dic["master"] as! String
            grupo.nombre = dic["nombre"] as! String
            grupo.reservado = false
            grupo.planes = planes
            
            dictionary = crearDiccionario()
            
            print("TEST: ",grupo.planes.count)
            
            insertFire(grupo: grupo)
            //DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(txtNombreGrupo.text!).setValue("grupo")
            
            DataHolder.sharedInstance.nombreGrupo = txtNombreGrupo.text!
            
        } else {
            //Si no llegamos al maximo, volvemos a cargar datos
            cargarInfo()
        }
        
    }
    
    func crearDiccionario() -> NSMutableDictionary {
        let dic: NSMutableDictionary! = NSMutableDictionary()
        
        let email = crearEmail()
        
        dic["nombre"] = txtNombreGrupo.text
        dic["master"] = email
        dic["fechaInicioEncuesta"] = crearFechaHoy()
        dic["reservado"] = false
        dic["planes"] = planes
        return dic
    }
    
    func crearFechaHoy() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        let myString = formatter.string(from: Date())
        return myString
    }
    
    func crearEmail() -> String {
        var email = Auth.auth().currentUser?.email
        email = email?.replacingOccurrences(of: ".", with: ";")
        return email!
    }
    
    func crearStringFromFecha(tipo:String) -> String {
        let fecha: String?
        
        if tipo == "inicio" {
            //fecha = txtDiaInicio.text! + "-" + txtMesInicio.text! + "-" + txtAnioInicio.text!
            fecha = txtAnioInicio.text! + "-" + txtMesInicio.text! + "-" + txtDiaInicio.text!
        } else {
            //fecha = txtDiaFin.text! + "-" + txtMesFin.text! + "-" + txtAnioFin.text!
            fecha = txtAnioFin.text! + "-" + txtMesFin.text! + "-" + txtDiaFin.text!
        }
        
        return fecha!
    }
    
    func crearStringFromFechaDMY(tipo:String) -> String {
        let fecha: String?
        
        if tipo == "inicio" {
            fecha = txtDiaInicio.text! + "-" + txtMesInicio.text! + "-" + txtAnioInicio.text!
            //fecha = txtAnioInicio.text! + "-" + txtMesInicio.text! + "-" + txtDiaInicio.text!
        } else {
            fecha = txtDiaFin.text! + "-" + txtMesFin.text! + "-" + txtAnioFin.text!
            //fecha = txtAnioFin.text! + "-" + txtMesFin.text! + "-" + txtDiaFin.text!
        }
        
        return fecha!
    }
    
    func loadLabels() {
        DispatchQueue.main.async {
            self.lblPrecio.text = self.plan.precio
            self.lblHotel.text = self.plan.hotel
            self.lblCiudad.text = self.plan.ciudad
            self.lblActividad.text = self.plan.actividad
        }
        
    }
    
    @objc func bajarTeclado() {
        txtDiaInicio.endEditing(true)
        txtDiaFin.endEditing(true)
        txtMesInicio.endEditing(true)
        txtMesFin.endEditing(true)
        txtAnioInicio.endEditing(true)
        txtAnioFin.endEditing(true)
        txtnumPersonas.endEditing(true)
        txtMinMoney.endEditing(true)
        txtMaxMoney.endEditing(true)
        txtNombreGrupo.endEditing(true)
        
    }
    
    func setStylesUI() {
        
        
        //TAPs
        let tap = UITapGestureRecognizer(target: self, action: #selector(bajarTeclado))
        self.view.addGestureRecognizer(tap)
    }
    
    
    
    
    func insertFire(grupo:Grupo){
        print("INSERTANDO")
        /*
        let p = grupo.planes
        grupo.planes.removeAll()
        DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(txtNombreGrupo.text!).setValue(grupo)
        */
        let dic = grupo.sacarDict()
        
        DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(txtNombreGrupo.text!).setValue(dic)
        
        performSegue(withIdentifier: "segueRandomAddParticipantes", sender: self)
    }
}














