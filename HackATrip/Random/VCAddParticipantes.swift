//
//  VCAddParticipantes.swift
//  HackATrip
//
//  Created by Charly Maxter on 21/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit

class VCAddParticipantes: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tablaUsuarios: UITableView!
    @IBOutlet weak var txtUser: UITextField!
    
    var participantes:NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        setStylesUI()
        tablaUsuarios.delegate = self
        tablaUsuarios.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addUsuario() {
        var email = txtUser.text
        email = email?.replacingOccurrences(of: ".", with: ";")
        participantes.add(email)
        tablaUsuarios.reloadData()
    }
    
    @IBAction func terminar(){
        DataHolder.sharedInstance.firDataBaseRef.child("grupos").child(DataHolder.sharedInstance.nombreGrupo!).child("participantes").setValue(participantes)
        
        //segue ir inicio
        performSegue(withIdentifier: "segueAddParticipantesMainChooser", sender: self)
    }
    
    func inyectarUsuarios(){
        for email in participantes {
            DataHolder.sharedInstance.firDataBaseRef.child("usuarios").child(email as! String).child(DataHolder.sharedInstance.nombreGrupo!).setValue(false)
        }
    }

    //INICIO FUNCIONES TABLA
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return participantes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        var texto = participantes[indexPath.row] as! String
        texto = texto.replacingOccurrences(of: ";", with: ".")
        
        cell.textLabel?.text = texto
        
        return cell
    }
    
    //FIN FUNCIONES TABLA
    
    @objc func bajarTeclado() {
        txtUser.endEditing(true)
        
    }
    
    func setStylesUI() {
        
        
        //TAPs
        let tap = UITapGestureRecognizer(target: self, action: #selector(bajarTeclado))
        self.view.addGestureRecognizer(tap)
    }

}
