//
//  Grupo.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit

class Grupo: NSObject {
    
    var nombre: String = ""
    var participantes: NSArray?
    var master: String = ""
    var reservado: Bool = false
    
    var planes = [Plan]()
    
    init(dic: NSDictionary, name: String) {
        nombre = name
        reservado = dic["reservado"] as! Bool
        //Array de emails de los participantes
        participantes = dic["participantes"] as? NSArray
        
        master = dic["master"] as! String
        
        let temp = dic["planes"] as! NSArray
        print("TEMP: ",temp)
        print(temp.count)
        for p in temp {
            let test:Plan = Plan.init(dic: p as! NSDictionary)
            planes.append(test)
        }
        
        
    }
    override init() {
        
    }
    
    func sacarDict() -> NSMutableDictionary {
        var res:NSMutableDictionary = NSMutableDictionary()
        var arrPlanes:NSMutableArray = NSMutableArray()
        var planBis:NSMutableDictionary = NSMutableDictionary()
        print("INICIO KEY")
        print(self.planes.count)
        for p:Plan in self.planes {
            
            planBis["actividad"] = p.actividad
            planBis["ciudad"] = p.ciudad
            
            planBis["hotel"] = p.hotel
            planBis["pais"] = p.pais
            planBis["precio"] = p.precio
            planBis["urlImagen"] = p.urlImagen
            planBis["votos"] = p.votos
            
            arrPlanes.add(planBis)
            
        }
        print("arrPlanes",arrPlanes.count)
        res["planes"] = arrPlanes
        res["master"] = self.master
        res["nombre"] = self.nombre
        res["reservado"] = self.reservado
        
        var arrPart:NSMutableArray = NSMutableArray()
        if participantes != nil {
            for p in self.participantes! {
                arrPart.add(p)
            }
            
            res["participantes"] = arrPart
        }
        return res
    }
    
}
