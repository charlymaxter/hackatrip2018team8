//
//  Plan.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit

class Plan: NSObject {

    var actividad: String = ""
    var ciudad: String = ""
    var pais: String = ""
    var fechaFin: Date?
    var fechaInicio: Date?
    var precio: String = ""
    var urlImagen: String = ""
    var hotel: String = ""
    var votos: Int = 0
    
    init(dic: NSDictionary) {
        
        //STRINGS
        actividad = dic["actividad"] as! String
        ciudad = dic["ciudad"] as! String
        pais = dic["pais"] as! String
        precio = dic["precio"] as! String
        urlImagen = dic["urlImagenPlan"] as! String
        hotel = dic["hotel"] as! String
        votos = (dic["votos"] as! Int)
        //FECHAS
        
        let dateFormatterSor = DateFormatter()
        dateFormatterSor.dateFormat = "dd-MM-yyyy"
        dateFormatterSor.timeZone = TimeZone(abbreviation: "UTC")
        
        fechaFin = dateFormatterSor.date(from: dic["fechaFin"] as! String)
        fechaInicio = dateFormatterSor.date(from: dic["fechaInicio"] as! String)
        
    }
    
}
