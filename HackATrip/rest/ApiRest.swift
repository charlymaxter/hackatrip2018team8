//
//  ApiRest.swift
//  HackATrip
//
//  Created by Charly Maxter on 21/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit

typealias ServiceResponse = (JSON, NSError?) -> Void

class ApiRest: NSObject {
    static let sharedInstance = ApiRest()
    
    fileprivate let path = "https://www.colonialnorte.es/api"
    fileprivate let allRandom = "/allRandom.php"

    
    
    func allRandom(_ num_personas: Int, min_dinero: Int, max_dinero: Int, fecha_inicio: String, fecha_fin: String, onCompletion: @escaping (JSON) -> Void ){
        var route = path + allRandom
        
        let postItems = "?num_personas=\(num_personas)&min_dinero=\(min_dinero)&max_dinero=\(max_dinero)&fecha_inicio="+fecha_inicio+"&fecha_fin="+fecha_fin
        
        route = route + postItems
        
        makeHTTPGetRequest(route, onCompletion: {json, err in
            
            if err != nil {
                //print(json)
                onCompletion(json as JSON)
            }
            
        })
        
        makeHTTPPostRequest(route, body: postItems, auth: "", onCompletion: { json, err in
            //TODO
            print("Pato")
            print(json)
            print(err)
            onCompletion(json as JSON)
        })
    }

    fileprivate func makeHTTPPostRequest(_ path: String, body: String, auth: String, onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: URL(string: path)!)
        // Set the method to POST
        request.httpMethod = "POST"
        // Set the POST body for the request
        request.httpBody = body.data(using: String.Encoding.utf8);
        print("inicio algo",body)
        if(auth != ""){
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("\(auth)", forHTTPHeaderField: "authorization")
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            print("la - ",data?.description)
            print("la - ",data?.debugDescription)
            
            print("la - ",response)
            print("la - ", error)
            do{
                if let jsonData = data {
                    let json:JSON = try JSON(data: jsonData)
                    print("RES: ",json)
                    //ERROR ESTA NIL, PROBLEMA
                    
                    onCompletion(json, nil)
                } else {
                    onCompletion(nil, error as! NSError)
                }
            }catch let err as NSError{
                print("asdfasdf",err)
            }
        })
        task.resume()
    }
    
    fileprivate func makeHTTPGetRequest(_ path: String, onCompletion: @escaping ServiceResponse) {
        print("ENTRE AQUI")
        let request = NSMutableURLRequest(url: URL(string: path)!)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: URLRequest(url: URL(string: path)!), completionHandler: {data, response, error -> Void in
            do{
                if let jsonData = data {
                    
                    let json:JSON = try JSON(data: jsonData)
                    onCompletion(json, nil)
                } else {
                    onCompletion(nil, error as! NSError)
                }
            }catch let err as NSError{
                print("SO SAAAAD",err)
            }
        })
        task.resume()
    }
    
}
