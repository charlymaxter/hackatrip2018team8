//
//  VCRegistro.swift
//  HackATrip
//
//  Created by Charly Maxter on 20/1/18.
//  Copyright © 2018 Team8. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class VCRegistro: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEdad: UITextField!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellido: UITextField!
    
    @IBOutlet weak var btnRegistrar: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        //SETEAR ESTILOS
        setStylesUI()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func registrar(){
        
        if (txtPassword.text?.count != 0) && (txtEmail.text?.count != 0){
            
            Auth.auth().createUser(withEmail: txtEmail.text!, password: txtPassword.text!) { (user, error) in
                
                if error != nil{
                    print("Error Registrando: ",error)
                } else {
                    //segueRegistroInitialChooser
                    self.performSegue(withIdentifier: "segueRegistroInitialChooser", sender: self)
                }
            }
        }else{
            
            print("Passwords no iguales o algo es menor de 6 caracteres")
        }
        
        
    }
    
    func setStylesUI(){
        
        
        
        //PARA PODER ESCONDER TECLADO
        let tap = UITapGestureRecognizer(target: self, action: #selector(esconderTeclado))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func esconderTeclado(){
        
        txtEmail?.endEditing(true)
        txtPassword?.endEditing(true)
        txtEdad?.endEditing(true)
        txtNombre?.endEditing(true)
        txtApellido?.endEditing(true)
    }
}
